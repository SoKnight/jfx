/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.event;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EventBusTest {

    @Test
    public void testBusRuns() {
        EventBus bus = new EventBus();
        String message = "message";
        String[] data = new String[1];
        bus.addListener(String.class, event -> data[0] = event);
        bus.fireEvent(message);
        assertEquals(message, data[0]);
    }

    @Test
    public void testBusUnsubscribe() {
        EventBus bus = new EventBus();
        String message = "message";
        String[] data = new String[1];
        EventRegistration<String> listener = bus.addListener(String.class, event -> data[0] = event);
        bus.fireEvent(message);
        listener.unsubscribe();
        bus.fireEvent("wrong message");
        assertEquals(message, data[0]);
    }

    @Test
    public void testBusUnsubscribe2() {
        EventBus bus = new EventBus();
        String message = "message";
        String[] data = new String[1];
        bus.addListener(String.class, new EventListener<>() {
            @Override
            public void handle(String event) {
                data[0] = event;
                bus.removeListener(this);
            }
        });
        bus.fireEvent(message);
        bus.fireEvent("wrong message");
        assertEquals(message, data[0]);
    }
}