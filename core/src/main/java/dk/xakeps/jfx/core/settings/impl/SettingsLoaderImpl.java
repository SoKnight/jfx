/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SoKnight
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.settings.impl;

import dk.xakeps.jfx.core.settings.Settings;
import dk.xakeps.jfx.core.settings.SettingsLoader;
import dk.xakeps.jfx.core.settings.SettingsPathException;
import javafx.beans.property.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class SettingsLoaderImpl implements SettingsLoader {
    private final Path root;

    public SettingsLoaderImpl(Path root) {
        this.root = root;
    }

    @Override
    public Settings load(Path path) throws IOException {
        Objects.requireNonNull(path, "The received path is null.");

        Path cfgDir;
        if (path.getNameCount() > 1) {
            cfgDir = root.resolve(path.getParent());
        } else {
            cfgDir = root;
        }
        Path file = cfgDir.resolve(path.getFileName().toString() + ".properties");
        if (!file.startsWith(root)) {
            throw new SettingsPathException(String.format(
                    "Wrong setting path provided. Root: %s, Path: %s, Resolved: %s", root, path, file
            ));
        }

        Properties properties = new Properties();
        if (Files.exists(file)) {
            try(Reader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
                properties.load(reader);
            }
        }

        return new SettingsImpl(file, properties);
    }
    
    private static class SettingsImpl implements Settings {
        private final Path file;
        private final Properties properties;
        private final Map<String, Property<?>> beanMap = new HashMap<>();
        
        private SettingsImpl(Path file, Properties properties) {
            this.file = file;
            this.properties = properties;
        }

        @Override
        public StringProperty getStringProperty(String key) {
            return (StringProperty) beanMap.computeIfAbsent(key, this::computeStringProperty);
        }

        private StringProperty computeStringProperty(String key) {
            String value = properties.getProperty(key);
            StringProperty property = new SimpleStringProperty();
            property.setValue(value);
            addListener(key, property);
            return property;
        }
        
        @Override
        public IntegerProperty getIntProperty(String key) {
            return (IntegerProperty) beanMap.computeIfAbsent(key, this::computeIntegerProperty);
        }

        private IntegerProperty computeIntegerProperty(String key) {
            String value = properties.getProperty(key);
            Integer integerValue = NumberConversions.getAsInteger(value);
            IntegerProperty property = new SimpleIntegerProperty();
            property.setValue(integerValue);
            addListener(key, property);
            return property;
        }
        
        @Override
        public LongProperty getLongProperty(String key) {
            return (LongProperty) beanMap.computeIfAbsent(key, this::computeLongProperty);
        }

        private LongProperty computeLongProperty(String key) {
            String value = properties.getProperty(key);
            Long longValue = NumberConversions.getAsLong(value);
            LongProperty property = new SimpleLongProperty();
            property.setValue(longValue);
            addListener(key, property);
            return property;
        }

        private void addListener(String key, Property<?> property) {
            property.addListener((observable, oldValue, newValue) -> onPropertyChanged(key, newValue));
        }

        private void onPropertyChanged(String key, Object newValue) {
            if (newValue == null) {
                properties.remove(key);
            } else {
                properties.put(key, newValue);
            }
        }

        @Override
        public boolean hasProperty(String key) {
            return properties.containsKey(key);
        }

        @Override
        public void save() throws IOException {
            try(BufferedWriter writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8)) {
                properties.store(writer, null);
            }
        }

    }

    
}
