/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.app;

import javafx.application.Preloader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AppPreloader extends Preloader {
    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        AnchorPane pane = new AnchorPane();
        Label label = new Label("Loading...");
        label.setScaleX(2);
        label.setScaleY(2);
        pane.getChildren().add(label);
        AnchorPane.setTopAnchor(label, 0d);
        AnchorPane.setRightAnchor(label, 0d);
        AnchorPane.setBottomAnchor(label, 0d);
        AnchorPane.setLeftAnchor(label, 0d);

        label.setAlignment(Pos.CENTER);

        Scene scene = new Scene(pane, 640, 480);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void handleApplicationNotification(PreloaderNotification info) {
        primaryStage.close();
    }
}
