/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.event;

public class EventRegistration<T> {
    private final EventBus eventBus;
    private final Class<T> eventType;
    final EventListener<T> listener;

    public EventRegistration(EventBus eventBus, Class<T> eventType, EventListener<T> listener) {
        this.eventBus = eventBus;
        this.eventType = eventType;
        this.listener = listener;
    }

    @SuppressWarnings("unchecked")
    void fire(Object event) {
        if (eventType.isInstance(event)) {
            listener.handle((T) event);
        }
    }

    public void unsubscribe() {
        eventBus.removeRegistration(this);
    }
}
