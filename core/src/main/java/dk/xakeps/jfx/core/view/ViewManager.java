/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.view;

import dk.xakeps.jfx.core.overlay.IndicatorOverlay;
import dk.xakeps.jfx.core.overlay.Overlay;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.concurrent.CompletableFuture;

public class ViewManager {
    private final ObjectProperty<View> currentView = new SimpleObjectProperty<>(this, "currentView");

    // root pane contains content pane and overlay pane
    // on action, content pane gets
    private final StackPane rootPane = new StackPane();
    private final StackPane contentPane = new StackPane();
    private final StackPane overlay = new StackPane();
    private boolean overlayEnabled = false;

    {
//        overlay.setBlendMode(BlendMode.OVERLAY);
        rootPane.getChildren().add(contentPane);
    }

    public ViewManager(Stage stage) {
        Scene scene = new Scene(rootPane, 640, 480);
        stage.setScene(scene);
    }

    public View getCurrentView() {
        return currentView.getValue();
    }

    public void setCurrentView(View view) {
        // only enable overlay if it was not enabled
        boolean savedState = overlayEnabled;
        if(!savedState) {
            enableOverlay(IndicatorOverlay.INSTANCE);
        }
        contentPane.getChildren().clear();
        contentPane.getChildren().add(view.getContent());
        currentView.set(view);
        if (!savedState) {
            disableOverlay();
        }
    }

    // Maybe something better?
    public <T> CompletableFuture<T> runWithOverlay(Overlay overlay, CompletableFuture<T> task) {
        enableOverlay(overlay);
        return task.whenCompleteAsync((t, throwable) -> disableOverlay(), Platform::runLater);
    }

    private void enableOverlay(Overlay overlayData) {
        if (overlayEnabled) {
            throw new IllegalStateException("Overlay already enabled");
        }
        overlayEnabled = true;
        contentPane.setEffect(new GaussianBlur());
        rootPane.getChildren().add(overlay);
        overlay.getChildren().add(overlayData.getContent());
    }

    private void disableOverlay() {
        if (!overlayEnabled) {
            throw new IllegalStateException("Overlay already disabled");
        }
        contentPane.setEffect(null);
        rootPane.getChildren().remove(overlay);
        overlay.getChildren().clear();
        overlayEnabled = false;
    }


    public ObservableValue<View> currentViewProperty() {
        return currentView;
    }
}
