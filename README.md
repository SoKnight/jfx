# How to build and launch:
1. Fork project
2. Run `mvn install` (!) to build project and it's dependencies
3. Run `mvn -pl :launcher javafx:run`