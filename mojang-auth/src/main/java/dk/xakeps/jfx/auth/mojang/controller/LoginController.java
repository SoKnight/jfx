/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.auth.mojang.MojangAuthModule;
import dk.xakeps.jfx.auth.mojang.exception.NoSelectedProfileException;
import dk.xakeps.jfx.core.auth.model.AccountModel;
import dk.xakeps.jfx.core.auth.model.AccountProfileModel;
import dk.xakeps.jfx.core.controller.AbstractController;
import dk.xakeps.jfx.auth.mojang.data.AuthRequest;
import dk.xakeps.jfx.auth.mojang.data.AuthResponse;
import dk.xakeps.jfx.auth.mojang.data.ErrorResponse;
import dk.xakeps.jfx.auth.mojang.model.UserModel;
import dk.xakeps.jfx.core.exception.AuthenticatorException;
import dk.xakeps.jfx.core.overlay.IndicatorAndLabelOverlay;
import javafx.application.Platform;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class LoginController extends AbstractController implements Initializable {

    private final UserModel userModel = new UserModel();

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final HttpClient httpClient = HttpClient.newBuilder().build();

    private MojangAuthModule authModuleImpl;

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private CheckBox rememberMeCheckBox;

    public void onLogIn(ActionEvent event) throws IOException {
        String username = userModel.getUsername();
        String password = userModel.getPassword();

        if (!validateInput(usernameField, passwordField)) {
            return;
        }

        // TODO: properly generate client token
        // TODO: extract business logic
        AuthRequest authRequest = new AuthRequest(username, password, "jfxtest", true);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://authserver.mojang.com/authenticate"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(authRequest)))
                .build();

        IndicatorAndLabelOverlay overlay = new IndicatorAndLabelOverlay();
        Label label = overlay.getLabel();

        label.setText("Loading...");

        Runnable authRunnable = () -> {
            try {
                HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

                if (send.statusCode() / 100 != 2) {
                    ErrorResponse errorResponse = objectMapper.readValue(send.body(), ErrorResponse.class);
                    Platform.runLater(() -> {
                        label.setText(errorResponse.getErrorMessage());
                        label.setTextFill(Color.RED);
                    });
                    Thread.sleep(1000L);
                } else {
                    AuthResponse authResponse = objectMapper.readValue(send.body(), AuthResponse.class);

                    final List<AccountProfileModel> availableProfiles = authResponse.getAvailableProfiles().stream()
                            .map(playerProfile -> new AccountProfileModel(playerProfile.getId(), playerProfile.getName()))
                            .collect(Collectors.toList());
                    final AccountProfileModel selectedProfile = availableProfiles.stream()
                            .filter(accountProfileModel -> accountProfileModel.getUserId().equals(authResponse.getSelectedProfile().getId()))
                            .findFirst()
                            .orElseThrow(NoSelectedProfileException::new);

                    Platform.runLater(() -> {
                        authModuleImpl.setAccountModel(
                                new AccountModel(
                                        authResponse.getUser().getUsername(), authResponse.getAccessToken(),
                                        authRequest.getClientToken(), availableProfiles, selectedProfile
                                )
                        );
                    });
                }
            } catch (IOException | InterruptedException ex) {
                throw new AuthenticatorException(ex);
            }
        };

        getViewManager().runWithOverlay(overlay, CompletableFuture.runAsync(authRunnable));
    }

    public UserModel getUserModel() {
        return userModel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameField.textProperty().bindBidirectional(userModel.usernameProperty());
        passwordField.textProperty().bindBidirectional(userModel.passwordProperty());
        rememberMeCheckBox.selectedProperty().bindBidirectional(userModel.rememberMeProperty());

        addValidationListener(usernameField, passwordField);
    }

    private boolean validateInput(TextField... fields) {
        boolean valid = true;

        for (TextField field : fields) {
            final String inputText = field.getText();
            PseudoClass error = PseudoClass.getPseudoClass("error");
            if (inputText == null || inputText.isEmpty()) {
                field.pseudoClassStateChanged(error, true);
                valid = false;
            } else {
                field.pseudoClassStateChanged(error, false);
            }
        }

        return valid;
    }

    private void addValidationListener(TextField... fields) {
        for (TextField field : fields) {
            field.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) { //unfocus
                    validateInput(field);
                }
            });
        }
    }

    public void setAuthModuleImpl(MojangAuthModule authModuleImpl) {
        this.authModuleImpl = authModuleImpl;
    }

    public void onMousePressedTextField(MouseEvent mouseEvent) {
        Node source = (Node) mouseEvent.getSource();
        source.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), false);
    }
}
