/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang;

import dk.xakeps.jfx.auth.mojang.controller.LoginController;
import dk.xakeps.jfx.core.auth.AuthModule;
import dk.xakeps.jfx.core.auth.model.AccountModel;
import dk.xakeps.jfx.core.settings.SettingsLoader;
import dk.xakeps.jfx.core.view.FXMLView;
import dk.xakeps.jfx.core.view.FXMLViewImpl;
import dk.xakeps.jfx.core.view.View;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;

public class MojangAuthModule implements AuthModule {

    private static final FXMLView LOGIN_VIEW = new FXMLViewImpl(MojangAuthModule.class.getResource("/login.fxml"));

    private final ObjectProperty<AccountModel> accountModel = new SimpleObjectProperty<>(this, "accountModel");
    private SettingsLoader settingsLoader;

    @Override
    public View getView() {
        ((LoginController) LOGIN_VIEW.getController()).setAuthModuleImpl(this);
        return LOGIN_VIEW;
    }

    public void setAccountModel(AccountModel model) {
        accountModel.setValue(model);
    }

    @Override
    public ObservableValue<AccountModel> accountModelProperty() {
        return accountModel;
    }

    @Override
    public void setSettingsLoader(SettingsLoader settingsLoader) {
        this.settingsLoader = settingsLoader;
    }
}
